<?php

namespace wwfrm\paypal_status;

use FrmEntry;
use FrmEntryMeta;
use function WWCore\exists;
use function wwfrm\update_setting;

const OPT = "payment_status_fid";

add_action('init', __NAMESPACE__ . '\\set_canceled');
add_action('wwfrm_settings', __NAMESPACE__ . '\\option_add');
add_filter('frm_form_options_before_update', function ($options, $values) {return update_setting($options, $values, OPT);}, 20, 2);

function set_canceled()
{
    $entry_key = exists($_GET, 'paypal_cancel');
    if (!$entry_key) {return;}

    $entry = FrmEntry::getOne($entry_key, true);
    $field_id = exists(get_option(OPT), exists($entry, 'form_id'));

    if ($field_id) {
        $status = FrmEntryMeta::add_entry_meta($entry->id, $field_id, null, "canceled");
    }
}

function option_add($values)
{
    $option = get_option(OPT);
    ?>
    <label for="<?=OPT?>"><strong>Payment Status Field ID</strong>
        <input id="<?=OPT?>" type="number" name="<?=OPT?>" value="<?=exists($option, $values['id'])?>">
    </label>
    <?php
}