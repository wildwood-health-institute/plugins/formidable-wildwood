<div class="destroy_partial" data-form_id="<?=$atts['form_id']?>">
    <p><strong>You seem to have an application in progress. Do you want to continue with your previous application?</strong></p>
    <div class="buttons frm_submit">
        <button class="destroy" data-form_id="<?=$atts['form_id']?>">Start Over</button>
        <button class="continue" type="submit">Continue</button>
    </div>
</div>
