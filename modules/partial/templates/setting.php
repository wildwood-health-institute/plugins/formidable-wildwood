<?php
namespace wwfrm\partial;

use function WWCore\exists;

$timeout = exists(get_option(TimeoutName), $atts['id']);
?>

<h3>Partial entries</h3>
<p class="frm_half frm_form_field">
    <label>
        <input type="checkbox" name="<?=Prefix . 'entry'?>" autocomplete="off" <?=$timeout ? 'checked' : ''?> /> Allow partial entries
    </label>
</p>
<p class="frm_half frm_form_field">
    <label>
    Expires after
    <input type="number" placeholder="2880" name="<?=Prefix . 'timeout'?>" value="<?=$timeout?>" autocomplete="off">
    minutes
    </label>
</p>