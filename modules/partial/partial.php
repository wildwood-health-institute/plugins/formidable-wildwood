<?php
namespace wwfrm\partial;

use FrmEntry;
use function WWCore\enqueue_maybe;
use function WWCore\exists;
use function WWCore\render;
use function wwfrm\get_option_name;
use function wwfrm\update_setting;

const Prefix = "wwfrm_partial_";
const ToggleName = "Partial Entry";
define(__NAMESPACE__ . "\\TimeoutName", prefix("timeout"));
const DefaultTimeout = 2880;

require "cron.php";

add_filter("wwfrm_toggles", __NAMESPACE__ . "\\add_toggle");

if (is_active_site()) {
    /*
     * Adds checkbox to form settings page to enable servce mode
     */
    add_action("wwfrm_settings", function ($values) {echo render(__FILE__, "setting", $values);});
    add_filter("frm_form_options_before_update", __NAMESPACE__ . "\\update_form_option", 20, 2);

    add_action("frm_display_form_action", __NAMESPACE__ . "\\set_partial_meta", 8, 3);
    add_filter("frm_validate_entry", __NAMESPACE__ . "\\save_draft", 10, 3);

    add_action("frm_pre_create_entry", __NAMESPACE__ . "\\pre_create_cleanup");
    add_action("frm_after_create_entry", __NAMESPACE__ . "\\update_cookie", 10, 2);
    add_action("frm_after_update_entry", __NAMESPACE__ . "\\update_cookie", 10, 2);
}

/**
 * Add toggle to form settings page
 *
 * @param array $toggles
 * @return array
 */
function add_toggle($toggles)
{
    $toggles[] = [
        "name" => ToggleName,
        "description" => "Allow non-logged-in users to save drafts of their submission",
    ];

    return $toggles;
}

/**
 * If partials are enabled for this site and form
 */
function is_active($form_id)
{
    return is_active_site() && is_active_form($form_id);
}

/**
 * If partials are enabled for this site
 */
function is_active_site()
{
    return (bool) get_option(get_option_name(ToggleName));
}

/**
 * If partials are enabled for this form
 *
 * @param int $form_id
 * @return boolean
 */
function is_active_form($form_id)
{
    return (bool) array_key_exists($form_id, (array) get_option(TimeoutName));
}

/**
 * Add Prefix to string
 */
function prefix($name)
{
    return Prefix . $name;
}

/**
 * Update partial status
 *
 * @param array $options
 * @param array $values
 * @return array
 */
function update_form_option($options, $values)
{
    // Exit early if not active sitewide
    if (!is_active_site()) {return $options;}

    $timeout = 0;

    // If partial is enabled for this form
    if ("on" == exists($values, prefix("entry"))) {
        $timeout = exists($values, prefix("timeout")) ?: DefaultTimeout;
    }

    $values[TimeoutName] = $timeout;
    return update_setting($options, $values, TimeoutName);
}

/**
 * Set cookie for this form
 *
 * @param int $form_id
 * @param mixed $value
 * @param int $expiration
 * @return void
 */
function set_cookie($form_id, $value, $expiration = null)
{
    // Get form timeout or use the default timeout
    $timeout = exists(get_option(TimeoutName), $form_id) ?: DefaultTimeout;
    $expiration = $expiration ?: time() + $timeout * 60;

    return setcookie(prefix($form_id), $value, $expiration, COOKIEPATH, COOKIE_DOMAIN, is_ssl(), true);
}

/**
 * Set values from partial before showing form
 *
 * @param array $params
 * @param array $fields
 * @param object $form
 * @return void
 */
function set_partial_meta($params, $fields, $form)
{
    // Exit early if not active for this site and form
    if (!is_active($params["form_id"])) {return;}

    $key = exists($_COOKIE, prefix($params["form_id"]));
    $entry = FrmEntry::getOne($key, true);

    // Exit early if entry does not exist or is not a partial
    if (!$entry || 1 != $entry->is_draft) {return;}

    // Add banner above form
    echo render(__FILE__, "partial", $params, false);
    enqueue_maybe(__FILE__, "partial", "css");
    enqueue_maybe(__FILE__, "partial", "js", [], false, true);

    // Copy the fields from the partial entry to the current form
    foreach ($fields as $field) {
        if (exists($entry->metas, $field->id)) {
            $field->default_value = $entry->metas[$field->id];
        }
    }
}

/**
 * Create or update partial entry
 *
 * @param array $errors
 * @param array $form
 * @return void
 */
function save_draft($errors, $form)
{
    if ($errors ||
        $form["frm_action"] != "create" ||
        !is_active($form["form_id"]) ||

        // Don't save partial if all fields are empty
        0 == count(array_filter($form["item_meta"]))) {
        return $errors;
    }

    // Set current entry to draft
    $form["is_draft"] = 1;

    $key = exists($_COOKIE, prefix($form["form_id"]));
    $entry = FrmEntry::getOne($key, true);

    if (!$key || !$entry) {
        $entry_id = FrmEntry::create($form);
        FrmEntry::getOne($entry_id, true);
    } else {
        // Set currrent entry id to that of the partial
        $form["id"] = $entry->id;
        FrmEntry::update($entry->id, $form);
    }

    return $errors;
}

/**
 * Delete partial before real entry is submitted to avoid duplicate keys
 *
 * @param array $values
 * @return array
 */
function pre_create_cleanup($values)
{
    // If submitted not just turned the page
    if (exists($values, "frm_skip_cookie")) {
        destroy_partial($values["form_id"], exists($values, "item_key"));
    }

    return $values;
}

/**
 * Destroy partial
 *
 * Can be called by php or AJAX
 *
 * @param int $form_id
 * @param string $key
 * @return void
 */
function destroy_partial($form_id, $key = null)
{
    // Pass key or get key from cookie (When called from AJAX)
    $key = $key ?: exists($_COOKIE, prefix($form_id));
    $entry = FrmEntry::getOne($key);

    // If active and entry is partial
    if (is_active($form_id) && exists($entry, "is_draft")) {
        // Destroy partial's cookie
        set_cookie($form_id, "", time() - (15 * 60));
        return (bool) FrmEntry::destroy($entry->id);
    }

    return false;
}

/**
 * Update the cookie key everytime a partial is updated
 *
 * @param int $entry_id
 * @param int $form_id
 * @return void
 */
function update_cookie($entry_id, $form_id)
{
    // Exit early if partial not active for site and form
    if (!is_active($form_id)) {return;}

    $entry = FrmEntry::getOne($entry_id);

    if ($entry && exists($entry, "is_draft")) {
        set_cookie($form_id, $entry->item_key);
    }
}
