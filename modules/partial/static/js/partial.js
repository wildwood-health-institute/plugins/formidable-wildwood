class DestroyPartialBtn extends EventFetch {
    constructor(button) {
        super(button, 'click')
        this.body.action = 'wwfrm'
        this.body.func = 'partial::destroy_partial'
        this.continue_button = button.parentNode.querySelector('.continue')
    }

    handler() {
        this.element.disabled = true
        this.continue_button.disabled = true

        const icon = document.createElement("span")
        icon.className = "dashicons dashicons-update"
        this.element.prepend(icon)

        return true
    }

    success() {
        location.replace(location.href)
    }

    error(error) {
        this.element.disabled = false
        this.continue_button.disabled = false

        console.error(error)
        window.alert('There was error discarding your previous submission. Please try again later.')
    }
}

document.querySelectorAll(".destroy_partial").forEach(alert => {
    const form = document.querySelector(`#frm_form_${alert.dataset.form_id}_container`)
    const classes = Array.from(form.classList).filter(c => c.includes("style"))
    classes.forEach(c => alert.classList.add(c))

    const destroy_button = alert.querySelector('.destroy')
    const continue_button = alert.querySelector('.continue')

    new DestroyPartialBtn(destroy_button)

    continue_button.addEventListener('click', () => {
        destroy_button.disabled = true
        continue_button.disabled = true

        const icon = document.createElement("span")
        icon.className = "dashicons dashicons-update"
        continue_button.prepend(icon)

        Object.assign(alert.style, {
            transition: 'opacity 400ms ease, height 400ms ease-in-out',
            height: `${alert.scrollHeight}px`,
            paddingBottom: 0,
            opacity: 0,
        })

        setTimeout(() => Object.assign(alert.style, {
            opacity: 0,
            height: 0
        }), 0);

        setTimeout(() => alert.remove(), 400);
    })
})