<?php
namespace wwfrm\partial\cron;

use const wwfrm\partial\DefaultTimeout;
use const wwfrm\partial\Prefix;
use const wwfrm\partial\TimeoutName;
use const wwfrm\ROOT;
use DateTime;
use FrmEntry;
use function wwfrm\partial\is_active_site;

const CronName = Prefix . "clean";

register_activation_hook(ROOT, __NAMESPACE__ . "\\activation");
register_deactivation_hook(ROOT, __NAMESPACE__ . "\\deactivation");
add_action(CronName, __NAMESPACE__ . "\\clean");

/**
 * Add cron jobs on plugin activation
 */
function activate()
{
    if (!wp_next_scheduled(CronName)) {
        wp_schedule_event(time(), "daily", CronName);
    }
}

/**
 * Clear cron jobs on plugin deactivation
 */
function deactivate()
{
    wp_clear_scheduled_hook(CronName);
}

/**
 * Clear partial entries that have expired
 */
function clean()
{
    $now = new DateTime();
    $options = get_option(TimeoutName);
    $site_status = is_active_site();

    foreach ($options as $form_id => $timeout) {
        $entries = FrmEntry::getAll([
            "form_id" => $form_id,
            "is_draft" => 1,
        ]);

        foreach ($entries as $entry) {
            $timeout = $timeout ?: DefaultTimeout;
            $expiration = new DateTime("$entry->updated_at + $timeout minutes");

            if (!$site_status || $now > $expiration) {
                FrmEntry::destroy($entry->id);
            }
        }
    }
}
