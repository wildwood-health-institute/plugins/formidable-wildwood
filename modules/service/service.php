<?php
namespace wwfrm\service;

use FrmAppHelper;
use FrmForm;
use function WWCore\enqueue_maybe;
use function WWCore\exists;
use function WWCore\render;

define(__NAMESPACE__ . '\OptName', 'frm-service-mode');

add_filter('wwfrm_toggles', __NAMESPACE__ . '\add_toggle');

if (get_option('wwfrm_service-mode')) {
    add_action('init', __NAMESPACE__ . '\add_cap_to_admin');

    add_action('wwfrm_settings', __NAMESPACE__ . '\add_form_option');
    add_filter('frm_form_options_before_update', __NAMESPACE__ . '\update_form_option', 20, 2);

    add_filter('frm_display_form_action', __NAMESPACE__ . '\form_before_display', 8, 3);
    add_filter('frm_get_paged_fields', __NAMESPACE__ . '\remove_form_breaks', 9, 2);

    add_filter('frm_validate_field_entry', __NAMESPACE__ . '\validate_field', 10, 4);

    add_action('show_user_profile', __NAMESPACE__ . '\add_option_to_profile');
    add_action('edit_user_profile', __NAMESPACE__ . '\add_option_to_profile');
    add_action('personal_options_update', __NAMESPACE__ . '\update_profile');
    add_action('edit_user_profile_update', __NAMESPACE__ . '\update_profile');
}

/**
 * Add to Wildwood global menu toggles
 *
 * @param array $toggles
 * @return void
 */
function add_toggle($toggles)
{
    $toggles[] = [
        'name' => 'Service Mode',
        'description' => 'Allow capable users to enable service mode on forms',
    ];
    return $toggles;
}

/**
 * Add service mode capability to administrators
 *
 * @return void
 */
function add_cap_to_admin()
{
    $role = get_role('administrator');
    $role->add_cap(OptName);
}

/**
 * Add service mode checkbox to profile page
 *
 * @param object $user
 * @return void
 */
function add_option_to_profile($user)
{
    if (!current_user_can('edit_users')) {
        return;
    }

    $admin = in_array('administrator', $user->roles);
    ?>
    <h3>Formidable Service Mode</h3>
    <table class="form-table"><tbody>
        <tr>
            <th>
                <label>Service Mode</label>
            </th>
            <td>
                <label for="frm-service-opt">
                    <input id="frm-service-opt" type="checkbox" name="<?=OptName?>" <?=is_valid_user($user->ID) ? 'checked' : ''?> <?=$admin ? 'disabled' : ''?>>
                    Can enable formidable service mode
                </label>
            </td>
        </tr>
    </tbody></table>
<?php
}

/**
 * Add or remove capability when profile is updated
 *
 * @param int $user_id
 * @return void
 */
function update_profile($user_id)
{
    if (!current_user_can('edit_user', $user_id)) {
        return;
    }

    $user = get_userdata($user_id);
    $value = (bool) exists($_POST, OptName);

    $value ? $user->add_cap(OptName) : $user->remove_cap(OptName);
}

/**
 * Check if user or current user is capable of service mode
 *
 * @param integer $user_id
 * @return boolean
 */
function is_valid_user($user_id = null)
{
    return $user_id ? user_can($user_id, OptName) : current_user_can(OptName);
}

/**
 * Check if form has service mode available
 *
 * @param integer $form_id
 * @return boolean
 */
function is_valid_form($form_id)
{
    $opt = get_option(OptName);
    return $opt ? in_array($form_id, $opt) : false;
}

/**
 * Check user, form, and browser state for service mode
 *
 * Requires both is_valid_form() and is_valid_user() return true and service-mode get param be truthy
 *
 * @param integer $form_id
 * @return boolean
 */
function is_valid($form_id)
{
    return exists($_GET, 'service-mode') && is_valid_form($form_id) && is_valid_user();
}

/**
 * Adds checkbox to form settings page to enable servce mode
 *
 * @param array $values
 * @return void
 */
function add_form_option($values)
{
    $opt = (array) get_option(OptName);
    ?>
        <h3>Service Mode</h3>
        <label for="<?=OptName?>">
            <input type="checkbox" value="1" id="<?=OptName?>" name="<?=OptName?>" <?=in_array($values['id'], $opt) ? 'checked="checked"' : ''?> /> Allow capable roles to enter service mode
        </label>
    <?php
}

/**
 * Update service mode setting per form
 *
 * @param array $options
 * @param array $values
 * @return array
 */
function update_form_option($options, $values)
{
    $opt = (array) get_option(OptName);

    if (isset($values[OptName]) && (!isset($values['id']) || !in_array($values['id'], $opt))) {
        $opt[] = $values['id'];
        update_option(OptName, $opt);
    } else if (!isset($values[OptName]) && isset($values['id']) && in_array($values['id'], $opt)) {
        $pos = array_search($values['id'], $opt);
        unset($opt[$pos]);
        update_option(OptName, $opt);
    }

    return $options;
}

/**
 * Render the pre-form html and unrequire all fields
 *
 * @param array $params
 * @param array $fields
 * @param object $form
 * @return void
 */
function form_before_display($params, $fields, $form)
{
    if (!is_valid_form($form->id) || !is_valid_user()) {return;}

    $filename = 'service';
    echo render(__FILE__, $filename, ['id' => $form->id], false);

    if (exists($_GET, 'service-mode')) {
        enqueue_maybe(__FILE__, $filename, 'css');

        foreach ($fields as $field) {
            $field->required = 0;
        }
    }
}

/**
 * Remove page breaks
 *
 * @param array $fields
 * @param integer $form_id
 * @return array
 */
function remove_form_breaks($fields, $form_id)
{
    $page_titles = FrmForm::getOne($form_id)->options['rootline_titles'];

    if (is_valid($form_id)) {
        remove_filter('frm_get_paged_fields', 'FrmProFieldsHelper::get_form_fields', 10, 3);

        foreach ($fields as $key => $f) {
            if ($f->type == 'break') {
                $title = $page_titles[$f->id];
                $fields[$key]->type = 'html';
                $fields[$key]->description = break_title($title);
            }
        }
    }

    return $fields;
}

/**
 * Remove blank validation for required fields
 *
 * @param array $errors
 * @param object $posted_field
 * @param all $posted_value
 * @param array $args
 * @return array
 */
function validate_field($errors, $posted_field, $posted_value, $args)
{
    if (!is_valid($posted_field->form_id)) {
        return $errors;
    }

    $error_key = 'field' . $args['id'];
    $blank_msg = $posted_field->field_options['blank'] ?: FrmAppHelper::get_settings()->blank_msg;

    if ($posted_field->required &&
        !$posted_value &&
        array_key_exists($error_key, $errors) &&
        $errors[$error_key] == $blank_msg
    ) {
        unset($errors[$error_key]);
    }

    return $errors;
}

/**
 * Create a page break title
 *
 * @param string|int $title
 * @return string
 */
function break_title($title)
{
    return "<p class='frm-service-break'><span class='dashicons dashicons-media-default'></span>$title</p>";
}