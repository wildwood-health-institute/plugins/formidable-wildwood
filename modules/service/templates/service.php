<?php
namespace wwfrm\service;

use FrmForm;
use function WWCore\exists;

$page_titles = FrmForm::getOne($atts['id'])->options['rootline_titles'];
$one_page = !(bool) exists($_GET, 'service-mode');
$prefix = $one_page ? 'En' : 'Dis';
unset($_GET['service-mode']);
$get = str_replace('=', '=', http_build_query($_GET, null, '&'));
$href = $one_page ? "?$get&service-mode=" . $one_page : "?$get";
?>

<div class="service-mode-before">
    <a href="<?=$href?>"><?=$prefix?>able service mode</a>
    <?php if (exists($_GET, 'service-mode') && $page_titles): ?>
    <?=break_title(exists($page_titles, 0))?>
    <?php endif;?>
</div>