<?php
/** Plugin name: Formidable Wildwood Addons
 *  Description: Modular plugin for commonly requested features
 *  Author: Daniel Atwood
 *  Author URI: https://atwood.io
 *  Plugin URI: https://gitlab.com/datwood/formidable-wildwood
 *  Version: 1.0
 */

namespace wwfrm;

use function WWCore\exists;
use function WWCore\fetch;
use function WWCore\render;

const ROOT = __FILE__;
define(__NAMESPACE__ . '\Prefix', __NAMESPACE__ . '_');

add_action('plugins_loaded', __NAMESPACE__ . '\load_modules');
add_action('frm_add_settings_section', __NAMESPACE__ . '\add_global_settings_section');
add_action('wp_ajax_wwfrm', function () {fetch(__NAMESPACE__);});
add_action('wp_ajax_nopriv_wwfrm', function () {fetch(__NAMESPACE__);});

/**
 * Require all modules
 */
function load_modules()
{
    foreach (glob(__DIR__ . '/modules/*') as $name) {
        $file = is_dir($name) ? "$name/" . basename($name) . ".php" : $name;
        require_once $file;
    }
}

add_filter('frm_add_form_settings_section', __NAMESPACE__ . '\add_settings_tab', 10);

function add_settings_tab($sections)
{
    $sections[] = array(
        'name' => 'Wildwood Settings',
        'anchor' => 'wildwood_settings',
        'function' => function ($values) {
            echo '<p class="howto">Settings to customize all active Wildwood Formidable options.</p>';
            echo '<div class="frm_grid_container">';
            do_action('wwfrm_settings', $values);
            echo '</div>';
        },
    );

    return $sections;
}

/**
 * Add new tab to formidable settings
 *
 * @param array $sections
 * @return void
 */
function add_global_settings_section($sections)
{
    $sections['wildwood'] = [
        'function' => __NAMESPACE__ . '\route',
        'icon' => 'frm_icon_font frm_toggle_on_icon',
    ];

    return $sections;
}

/**
 * Display settings and proccess form
 *
 * @return void
 */
function route()
{
    $action = exists($_REQUEST, 'frm_action', 'action');
    // $action = FrmAppHelper::get_param($action);

    if ($action == 'process-form') {
        process_form();
    }

    echo render(__FILE__, 'global_options');
}

/**
 * Update sitewide toggles and do wwfrm_update_settings
 *
 * @return void
 */
function process_form()
{
    $toggles = [];
    $toggles = apply_filters('wwfrm_toggles', $toggles);

    foreach ($toggles as $option) {
        if (!exists($option, 'name')) {continue;}

        $name = Prefix . sanitize_title($option['name']);
        $value = exists($_POST, $name) ? 1 : 0;
        update_option($name, $value, true);
    }

    do_action('wwfrm_update_settings', $_POST);
}

function get_option_name($name)
{
    return Prefix . sanitize_title($name);
}

function update_setting($options, $values, $option_name)
{
    $form_id = $values['id'];
    $option = (array) get_option($option_name);
    $submitted_value = exists($values, $option_name);

    if (!$submitted_value) {
        unset($option[$form_id]);
    } else {
        $option[$form_id] = $submitted_value;
    }

    update_option($option_name, $option);
    return $options;
}
