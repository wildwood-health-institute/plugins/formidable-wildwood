<?php
namespace wwfrm;

use function WWCore\exists;

$toggles = [];
$toggles = apply_filters('wwfrm_toggles', $toggles);

?>
<p class="howto">Global settings for Wildwood Plugins</p>
<h3>Sitewide Toggles</h3>
<?php foreach ($toggles as $option) {
    if (!exists($option, 'name')) {continue;}

    $name = Prefix . sanitize_title($option['name']);
    $desc = exists($option, 'description');
    ?>
    <p>
        <label>
            <input type="checkbox" name="<?=$name?>" <?=get_option($name) ? 'checked' : ''?>>
            <?=$option['name']?>
            <?php if ($desc): ?>
            <span class="frm_help frm_icon_font frm_tooltip_icon" data-original-title="<?=$desc?>"></span>
            <?php endif;?>
        </label>
    </p>
<?php }

do_action('wwfrm_global_settings');